//
// Created by Ivan on 24.12.2021.
//

#include "tests.h"
#include "mem.h"
#include "mem_internals.h"

static void *heap;

bool start_tests() {
    heap = heap_init(400);
    if (test1() && test2() && test3() && test4() && test5()) {
        return true;
    } else {
        return false;
    }
}

bool test1() {
    if (heap == NULL) {
        printf("Test 1 failed\n");
        return false;
    } else {
        printf("Test 1 complete\n");
        return true;
    }
}

bool test2() {
    bool ret = false;
    void *block1 = _malloc(100);
    void *block2 = _malloc(100);

    if (!block_get_header(block1)->is_free && !block_get_header(block2)->is_free) {
        _free(block1);
        if (block_get_header(block1)->is_free) {
            if (!block_get_header(block2)->is_free) {
                _free(block2);
                printf("Test 2 complete\n");
                ret = true;
            } else {
                printf("Test 2 failed. Second block set free too\n");
                ret = false;
            }
        } else {
            printf("Test 2 failed. Block not set free\n");
            ret = false;
        }
    } else {
        printf("Test 2 failed. Not both blocks free\n");
        ret = false;
    }
    return ret;
}

bool test3() {
    bool ret = false;
    void *block1 = _malloc(100);
    void *block2 = _malloc(100);
    void *block3 = _malloc(100);
    if (!block_get_header(block1)->is_free && !block_get_header(block2)->is_free &&
        !block_get_header(block3)->is_free) {

        _free(block1);
        _free(block2);
        if (block_get_header(block1)->is_free && block_get_header(block2)->is_free) {
            if (!block_get_header(block3)->is_free) {
                _free(block3);
                printf("Test 3 complete\n");
                ret = true;
            } else {
                printf("Test 3 failed. Third block set free too\n");
                ret = false;
            }
        } else {
            printf("Test 3 failed. Not both blocks set free\n");
            ret = false;
        }

    } else {
        printf("Test 3 failed. Not all blocks free\n");
        ret = false;
    }
    return ret;
}

bool test4() {
    bool ret = false;
    void *block1 = _malloc(100);
    void *block2 = _malloc(10000);

    if (!(block_get_header(block1)->is_free) && !(block_get_header(block2)->is_free)) {
        printf("Test 4 complete\n");
        ret = true;
    } else {
        printf("Test 4 failed\n");
        ret = false;
    }
    return ret;
}

static inline struct block_header *block_by_allocated_data(void *data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(
    struct block_header, contents));
}

static void mmap_area(size_t length, void *addr) {
    void *pustoy_i_nenushniy_ukazatel = mmap(addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, 0, 0);
//    линтеру нужно чтобы я что-то делал с результатом этой функции. Но нужно это только ему
    printf("%p - чисто по приколу\n", pustoy_i_nenushniy_ukazatel);
}

bool test5() {
    bool ret = false;
    void *block1 = _malloc(100000);
    struct block_header *address = heap;
    while (address != NULL && !address->next)
        address = address->next;

    uint8_t *total = (uint8_t * )(8175 * ((size_t) address / 8175 + ((size_t) address % 8175 > 1)));

    mmap_area(200000, total);

    if (address == block_by_allocated_data(block1)) {
        printf("Test 5 failed\n");
        ret = false;
    } else {
        printf("Test 5 complete\n");
        ret = true;
    }
    return ret;
}

