//
// Created by Ivan on 24.12.2021.
//

#ifndef _TESTS_H
#define _TESTS_H

#include "stdbool.h"

bool start_tests();
bool test1();
bool test2();
bool test3();
bool test4();
bool test5();

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
